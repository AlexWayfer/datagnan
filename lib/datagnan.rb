## Oga for HTML parsing (Yeah, no Nokogiri!)
require 'oga'

class Datagnan

	attr_accessor :doc, :vars

	## Constructor
	# @param [String] template
	#   HTML-string or path to .html-template file
	# @param [Hash] options
	#   options for parsing and filling the template
	# @param [Hash] locals
	#   local variables for filling the template
	def initialize(template, options = {}, local_vars = {})
		template = File.read(template) if File.exist?(template)
		@doc = Oga.parse_html template
		## debug
		# puts "-- @doc = #{@doc}"
		@options = {}
		@options[:attrs_sep] = options[:attrs_sep] || ";"
		@options[:path_filters] = options[:path_filters] || {}
		@scope = options[:scope] || self
		@vars = (options[:local_vars] || {}).merge local_vars
		@vars.merge! Hash[@scope.instance_variables.map { |name| [name.to_s, @scope.instance_variable_get(name)] } ] unless @scope.nil?
		## debug
		# puts "-- Datagnan.new ( @vars = #{@vars} )"
	end

	## Create Datagnan instance and return this.
	## Can receive implicity block for 'yield.
	## Like File.open(template)
	# @param [String] template
	#   HTML-string or path to .html-template file
	# @param [Hash] options
	#   options for parsing and filling the template
	# @param [Hash] locals
	#   local variables for filling the template
	def self.open(template, options = {}, local_vars = {})
		obj = Datagnan.new(template, options, local_vars).write
		yield obj if block_given?
		return obj
	end

	## Create Datagnan instance and return the filling with variables HTML-template as String.
	## Like File.read(template)
	# @param [String] template
	#   HTML-string or path to .html-template file
	# @param [Hash] options
	#   options for parsing and filling the template
	# @param [Hash] locals
	#   local variables for filling the template
	def self.read(template, options = {}, local_vars = {})
		Datagnan.new(template, options, local_vars).write.read
	end

	## Return HTML-String. As File.new.read
	def read
		## debug
		# puts "-- Datagnan.read ( @doc = #{@doc} )"
		@doc.to_xml
	end

	## Parse and replace
	## Like File.new.write
	# @param [Hash] vars
	#   variables for filling the template
	# @param [Oga::XML::Document] root
	#   HTML-element for fill it
	def write(vars = {}, root = nil)
		## debug
		# puts "Datagnan.write ( vars = #{vars} )"
		# puts "Datagnan.write ( @doc = #{@doc} )"
		## use global if empty
		vars = @vars if vars.is_a?(Hash) && vars.empty?
		root = @doc if root.nil?
		## debug
		# puts "-- Datagnan.write ( vars = #{vars.inspect} )"
		# puts "-- Datagnan.write ( root = #{root.to_xml} )"
		## replace data-each
		data_same(vars, root)
		data_each(vars, root)
		## debug
		# puts "-- Datagnan.write after each ( vars = #{vars} )"
		# puts "-- Datagnan.write after each ( root = #{root.to_xml} )"
		## replace data-when
		data_when(vars, root)
		data_when_not(vars, root)
		## debug
		# puts "-- Datagnan.write after when ( vars = #{vars} )"
		# puts "-- Datagnan.write after when ( root = #{root.to_xml} )"
		## replace data-var
		data_var(vars, root)
		## debug
		# puts "Datagnan.write after vars ( vars = #{vars} )"
		# puts "Datagnan.write after vars ( root = #{root.to_xml} )"
		## replace data-attrs
		data_attrs(vars, root)
		## debug
		# puts "Datagnan.write after attrs ( vars = #{vars} )"
		# puts "Datagnan.write after attrs ( root = #{root.to_xml} )"
		## replace data-attr-*
		data_attr(vars, root)
		## debug
		# puts "Datagnan.write after attr ( vars = #{vars} )"
		# puts "Datagnan.write after attr ( root = #{root.to_xml} )"
		## result
		return self
	end

private

	## check class exists
	def class_exists?(class_name)
		klass = Module.const_get(class_name)
		return klass.is_a?(Class)
	rescue NameError
		return false
	end

	## find value in vars and scope
	def find_value(path, vars)
		## recursive access to property
		## debug
		# puts "-- Datagnan.find_value ( path = #{path} )"
		# puts "-- Datagnan.find_value ( vars = #{vars} )"
		# beginning_time = Time.now
		var = vars

		## path_filters for replace
		## Example:
		## path => '/foo/bar'
		## :path_filters = { /^\// => '', '/' => '.' }
		## path => 'foo.bar'
		@options[:path_filters].each { |key, val| path.gsub!(key, val) }

		dot_keys = path.split('.')
		while dot_keys.size > 0 do
			dot_key = dot_keys.delete_at(0)
			## debug
			# puts "-- Datagnan.find_value drop_while ( dot_key = #{dot_key} )"
			# puts "-- Datagnan.find_value drop_while ( var.has_key?(dot_key) = #{var.has_key?(dot_key)} )" if var.is_a?(Hash)

			if var.is_a?(Hash) and var.has_key?(dot_key)
				# (var.has_key?(dot_key) ?
				# 	true : false
				# ) :
				# false
				var = var[dot_key]
			else
				begin
					## debug
					# puts "-- Datagnan.find_value before eval var.instance_eval"
					# puts "-- Datagnan.find_value before eval ( var.instance_eval(dot_key) = #{var.instance_eval(dot_key)} )"
					var = var.instance_eval(dot_key)
				rescue SyntaxError, TypeError, NameError => e
					begin
						## debug
						# puts "-- Datagnan.find_value before eval var.instance_eval path.split"
						# puts "-- Datagnan.find_value before eval ( var.instance_eval(\"#{dot_key}.#{dot_keys[0]}\") = #{var.instance_eval("#{dot_key}.#{dot_keys[0]}")} )"
						var = var.instance_eval("#{dot_key}.#{dot_keys[0]}")
						# puts "-- Datagnan.find_value after eval var.instance_eval path.split dot_keys.delete"
						dot_keys.delete_at(0)
					rescue SyntaxError, TypeError, NameError => e
						begin
							## debug
							# puts "-- Datagnan.find_value before eval var.instance_eval path.split all"
							# puts "-- Datagnan.find_value before eval ( var.instance_eval(\"#{dot_key}.#{dot_keys.join('.')}\") = #{var.instance_eval("#{dot_key}.#{dot_keys.join('.')}")} )"
							var = var.instance_eval("#{dot_key}.#{dot_keys.join('.')}")
							# puts "-- Datagnan.find_value after eval var.instance_eval path.split all dot_keys.delete"
							dot_keys.clear
						rescue SyntaxError, TypeError, NameError => e
							begin
								## debug
								# puts "-- Datagnan.find_value before eval @scope.instance_eval('var.')"
								# puts "-- Datagnan.find_value before eval ( 'var.'+dot_key = #{'var.'+dot_key} )"
								# puts "-- Datagnan.find_value before eval ( @scope.instance_eval('var.'+dot_key) = #{@scope.instance_eval('var.'+dot_key)} )"
								var = @scope.instance_eval('var.'+dot_key)
							rescue SyntaxError, TypeError, NameError => e
								begin
									## debug
									# puts "-- Datagnan.find_value before eval @scope.instance_eval('var.') path.split"
									# puts "-- Datagnan.find_value before eval ( var = #{var} )"
									# puts "-- Datagnan.find_value before eval ( \"var.#{dot_key}.#{dot_keys[0]}\" = #{"var.#{dot_key}.#{dot_keys[0]}"} )"
									# puts "-- Datagnan.find_value before eval ( @scope.instance_eval(\"var.#{dot_key}.#{dot_keys[0]}\") = #{@scope.instance_eval("var.#{dot_key}.#{dot_keys[0]}")} )"
									var = @scope.instance_eval("var.#{dot_key}.#{dot_keys[0]}")
									# puts "-- Datagnan.find_value after eval @scope.instance_eval('var.') path.split dot_keys.delete"
									dot_keys.delete_at(0)
								rescue SyntaxError, TypeError, NameError => e
									begin
										## debug
										# puts "-- Datagnan.find_value before eval @scope.instance_eval('var.') path.split all"
										# puts "-- Datagnan.find_value before eval ( var = #{var} )"
										# puts "-- Datagnan.find_value before eval ( \"var.#{dot_key}.#{dot_keys.join('.')}\" = #{"var.#{dot_key}.#{dot_keys.join('.')}"} )"
										# puts "-- Datagnan.find_value before eval ( @scope.instance_eval(\"var.#{dot_key}.#{dot_keys.join('.')}\") = #{@scope.instance_eval("var.#{dot_key}.#{dot_keys.join('.')}")} )"
										var = @scope.instance_eval("var.#{dot_key}.#{dot_keys.join('.')}")
										# puts "-- Datagnan.find_value after eval @scope.instance_eval('var.') path.split all dot_keys.delete"
										dot_keys.clear
									rescue SyntaxError, TypeError, NameError => e
										begin
											## debug
											# puts "-- Datagnan.find_value before eval @scope.instance_eval"
											# puts "-- Datagnan.find_value before eval ( @scope.instance_eval(dot_key) = #{@scope.instance_eval(dot_key)} )"
											var = @scope.instance_eval(dot_key)
										rescue SyntaxError, TypeError, NameError => e
											begin
												## debug
												# puts "-- Datagnan.find_value before eval @scope.instance_eval path.split"
												# puts "-- Datagnan.find_value before eval ( \"#{dot_key}.#{dot_keys[0]}\" = #{"#{dot_key}.#{dot_keys[0]}"} )"
												# puts "-- Datagnan.find_value before eval ( @scope.instance_eval(\"#{dot_key}.#{dot_keys[0]}\") = #{@scope.instance_eval("#{dot_key}.#{dot_keys[0]}")} )"
												var = @scope.instance_eval("#{dot_key}.#{dot_keys[0]}")
												# puts "-- Datagnan.find_value after eval @scope.instance_eval path.split dot_keys.delete"
												dot_keys.delete_at(0)
											rescue SyntaxError, TypeError, NameError => e
												begin
													## debug
													# puts "-- Datagnan.find_value before eval @scope.instance_eval path.split all"
													# puts "-- Datagnan.find_value before eval ( \"#{dot_key}.#{dot_keys.join('.')}\" = #{"#{dot_key}.#{dot_keys.join('.')}"} )"
													# puts "-- Datagnan.find_value before eval ( @scope.instance_eval(\"#{dot_key}.#{dot_keys.join('.')}\") = #{@scope.instance_eval("#{dot_key}.#{dot_keys.join('.')}")} )"
													var = @scope.instance_eval("#{dot_key}.#{dot_keys.join('.')}")
													# puts "-- Datagnan.find_value after eval @scope.instance_eval path.split all dot_keys.delete"
													dot_keys.clear
												rescue SyntaxError, TypeError, NameError => e
													var = nil
												end
											end
										end
									end
								end
							end
						end
					end
				end
			end
			## debug
			# puts "-- Datagnan.find_value split result ( var = #{var} )"
			break if var.nil?
		end
		## debug
		# puts "-- Datagnan.find_value after drop_while ( var = #{var} )"
		# puts "-- Datagnan.find_value after drop_while ( dot_keys = #{dot_keys} )"
		# puts "-- Datagnan.find_value before eval ( dot_keys = #{dot_keys} )"

		## debug
		# puts "-- Datagnan.find_value result ( var = #{var} )"
		# end_time = Time.now
		# puts "Time elapsed for find_value #{(end_time - beginning_time)*1000} milliseconds"
		var
	end

	## node content
	def data_var(vars, root)
		## debug
		# puts "-- Datagnan.data_var ( vars = #{vars} )"
		# puts "-- Datagnan.data_var ( root.to_xml = #{root.to_xml} )"
		root.css('*[data-var]').each do |node|

			var = find_value(node.get('data-var'), vars)

			unless var.nil?
				begin
					node.children = Oga.parse_html(var.to_s).children
				rescue
					node.inner_text = var.to_s
				end
				node.unset('data-var')
			end
			## TODO: if var not exist remember it (maybe return to model)
		end
		## debug
		# puts "Datagnan.data_var end ( root = #{root} )"
	end

	## node attributes (DOMtempl-comaptible)
	# @since 1.2.0
	def data_attr(vars, root)
		root.xpath("//@*[starts-with(name(), 'data-attr-')]").each do |attr|
			node = attr.element

			key = attr.name.sub(/^data\-attr\-/, '')
			val = attr.value

			var = find_value(val, vars)
			## debug
			# puts "-- Datagnan.data_attr each after find_value ( var = #{var} )"
			# puts "-- Datagnan.data_attr each after find_value ( var.to_s = #{var.to_s} )"

			unless var.nil? || var == false
				## debug
				# puts "-- Datagnan.data_attr each before insert ( node.to_xml = #{node.to_xml} )"
				node.unset(key)
				node.attributes.insert(
					node.attributes.index(attr),
					Oga::XML::Attribute.new(
						:name => key,
						:value => var.to_s,
						:element => node
					)
				)
				## debug
				# puts "-- Datagnan.data_attr each after insert ( node.to_xml = #{node.to_xml} )"
			end
			node.unset(attr.name)
			## debug
			# puts "-- Datagnan.data_attr each ( node.to_xml = #{node.to_xml} )"
		end
		## debug
		# puts "-- Datagnan.data_attr after ( root.to_xml = #{root.to_xml} )"
	end

	## node attributes
	def data_attrs(vars, root)
		## debug
		# puts "-- Datagnan.data_attrs ( vars = #{vars} )"
		# puts "-- Datagnan.data_attrs ( root = #{root.to_xml} )"
		# puts "-- Datagnan.data_attrs ( @options[:attrs_sep] = #{@options[:attrs_sep]} )"
		root.css('*[data-attrs]').each do |node|
			## debug
			# puts "-- Datagnan.data_attrs each ( node = #{node} )"
			# puts "-- Datagnan.data_attrs each ( node.to_xml = #{node.to_xml} )"
			node_data_attrs = node.attribute('data-attrs')
			node_data_attrs.value.split(@options[:attrs_sep]).each do |attr|
				key_val = attr.split(':')
				key = key_val[0]
				val = key_val.size.odd? ? key_val[0] : key_val[1]
				## debug
				# puts "-- Datagnan.data_attrs each attr ( key = #{key}, val = #{val} )"
				# puts "-- Datagnan.data_attrs each attr ( vars = #{vars} )"
				var = find_value(val, vars)
				## debug
				# puts "-- Datagnan.data_attrs each attr ( var = #{var} )"

				unless var.nil? || var == false
					node.unset(key)
					node.attributes.insert(
						node.attributes.index(node_data_attrs),
						Oga::XML::Attribute.new(
							:name => key,
							:value => var.to_s,
							:element => node
						)
					)
				end
				# puts "-- Datagnan.data_attrs each ( node.to_xml = #{node.to_xml} )"
				## TODO: if attr not exist remember it (maybe return to model)
			end
			node.unset('data-attrs')
		end
	end

	## fill template by array
	def data_each(vars, root)
		## debug
		# puts "-- Datagnan.data_each ( vars = #{vars} )"
		# puts "-- Datagnan.data_each ( root = #{root.to_xml} )"
		# puts "-- Datagnan.data_each ( root.css('[data-each]') = #{root.css('[data-each]').inspect} )"
		root.css('*[data-each]').each do |node|

			var = find_value(node.get('data-each'), vars)

			unless var.nil?
			# --------
				node.unset('data-each')
				var.each do |key, item|
					item ||= key
					# puts "-- Datagnan.data_each each ( item = #{item} )"
					# puts "-- Datagnan.data_each each ( item.inspect = #{item.inspect} )"
					new_node = Oga.parse_html(node.to_xml)
					# puts "-- Datagnan.data_each each ( node after clone = #{node} )"
					# puts "-- Datagnan.data_each each ( node.to_xml after clone = #{node.to_xml} )"
					# puts "-- Datagnan.data_each each ( node.parent.to_xml = #{node.parent.to_xml} )"
					# puts "-- Datagnan.data_each each ( new_node = #{new_node} )"
					# puts "-- Datagnan.data_each each ( new_node.to_xml = #{new_node.to_xml} )"
					write(item, new_node)
					# puts "-- Datagnan.data_each each ( new_node after write = #{new_node} )"
					# puts "-- Datagnan.data_each each ( new_node.to_xml after write = #{new_node.to_xml} )"
					# puts "-- Datagnan.data_each each ( node after write= #{node} )"
					# puts "-- Datagnan.data_each each ( node.to_xml after write= #{node.to_xml} )"
					node.before(new_node.children.pop) unless new_node.children.empty?
					# puts "-- Datagnan.data_each each ( node = #{node} )"
					# puts "-- Datagnan.data_each each ( node.to_xml after insert = #{node.to_xml} )"
					# puts "-- Datagnan.data_each each ( node.parent = #{node.parent.to_xml} )"
				end
			end
			node.remove
		end
	end

	## destroy
	def data_same(vars, root)
		## debug
		# puts "Datagnan.data_same ( vars = #{vars} )"
		# puts "Datagnan.data_same ( root = #{root} )"
		root.css('*[data-same]').each do |node|
			node.remove
		end
	end

	## display if true
	def data_when(vars, root)
		## debug
		# puts "-- Datagnan.data_when ( vars = #{vars} )"
		# puts "-- Datagnan.data_when ( root.to_xml = #{root.to_xml} )"
		root.css('*[data-when]').each do |node|

			var = node.get('data-when').split(@options[:attrs_sep]).each do |key|
				## debug
				# puts "-- Datagnan.data_when each split ( key = #{key} )"
				var = find_value(key, vars)
				# puts "-- Datagnan.data_when each split ( var = #{var} )"
				break var unless var
			end
			## debug
			# puts "-- Datagnan.data_when each ( node.to_xml = #{node.to_xml} )"
			# puts "-- Datagnan.data_when each ( var = #{var} )"

			## allow for nil
			var ? node.unset('data-when') : node.remove
			## TODO: if var not exist remember it (maybe return to model)
		end
		## debug
		# puts "Datagnan.data_when end ( root = #{root} )"
	end

	## display if false
	# @since 1.1.0
	def data_when_not(vars, root)
		## debug
		# puts "Datagnan.data_when_not ( vars = #{vars} )"
		# puts "Datagnan.data_when_not ( root = #{root} )"
		root.css('*[data-when-not]').each do |node|

			var = node.get('data-when-not').split(@options[:attrs_sep]).each do |key|
				## debug
				# puts "-- Datagnan.data_when_not each split ( key = #{key} )"
				var = find_value(key, vars)
				# puts "-- Datagnan.data_when_not each split ( var = #{var} )"
				break var unless var
			end

			## allow for nil
			var ? node.remove : node.unset('data-when-not')
			## TODO: if var not exist remember it (maybe return to model)
		end
		## debug
		# puts "Datagnan.data_when_not end ( root = #{root} )"
	end

end

## Helper function (for Sinatra, primarily)
# @param [String] template
#   path to .html-template file
# @param [Hash] options
#   options for parsing and filling the template
# @param [Hash] locals
#   local variables for filling the template
def datagnan(templates, options = {}, local_vars = {})
	## debug
	# puts "Start datagnan"
	# beginning_time = Time.now
	## default options
	options[:scope] ||= self
	options[:views_dir] = File.realpath(options[:views_dir] || (options[:scope].settings.views if options[:scope].respond_to? :settings) || ("./views" if File.exist? "./views") || "./")
	options[:layout] = File.join(options[:views_dir], options[:layout] || 'layout.html')
	vars = (options[:local_vars] || {}).merge local_vars

	html = ""
	templates = [templates] unless templates.is_a? Array
	templates.each do |template|
		## prepare template filename
		template_file = File.join(options[:views_dir], template.to_s)
		template_file += '.html' unless File.exist? template_file
		template = template_file if File.exist? template_file

		## render template
		html += Datagnan.read(template, options, vars)
		## debug
		# puts "-- datagnan ( template = #{html} )"
	end

	## check for layout and wrap template if exist
	if File.exist? options[:layout]
		vars['template'] = { 'html' => html }
		html = Datagnan.read(options[:layout], options, vars)
		## debug
		# puts "-- datagnan ( layout = #{html} )"
	end

	## debug
	# end_time = Time.now
	# puts "Time elapsed for datagnan #{(end_time - beginning_time)*1000} milliseconds"

	return html
end